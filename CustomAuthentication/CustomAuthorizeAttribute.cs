﻿using Dapper;
using RegisterWebApp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RegisterWebApp.CustomAuthentication
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] allowedroles;
        private readonly string _conString;

        public CustomAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
            _conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            var username  =Convert.ToString(httpContext.Session["Username"]);

            if (!string.IsNullOrEmpty(username))
            {
                var roles = GetRoles(username);

                if (roles == null)
                    return authorize;

                authorize = roles.Any(r => this.allowedroles.Contains(r));
            }
               
            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
               new RouteValueDictionary
               {
                    { "controller", "Home" },
                    { "action", "UnAuthorized" }
               });
        }

        private IEnumerable<string> GetRoles(string userName)
        {
            using (IDbConnection _db = new SqlConnection(_conString))
            {
                var userRole = _db.QueryFirstOrDefault<UserRolesModel>("GetUserRole", new { username = userName },
                                commandType: CommandType.StoredProcedure);

                if (userRole != null && !string.IsNullOrEmpty(userRole.Roles))
                {
                    return userRole.Roles.Split(',');
                }
            }

            return null;
        }
    }
}