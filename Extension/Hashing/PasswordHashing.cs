﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;

namespace RegisterWebApp.Extension.Hashing
{
    public static class PasswordHashing
    {
        public static string GetPasswordHashing(this string password)
        {
            // Generate a random salt
            // Generate a random salt
            byte[] salt;
            using (var rng = new RNGCryptoServiceProvider())
            {
                salt = new byte[128 / 8]; // 128 bits
                rng.GetBytes(salt);
            }

            // Derive a 256-bit subkey using PBKDF2 with the salt
            using (var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 100000, HashAlgorithmName.SHA256))
            {
                byte[] hashBytes = pbkdf2.GetBytes(256 / 8); // 256 bits

                // Combine salt and hash and return as a single string
                byte[] combinedBytes = new byte[salt.Length + hashBytes.Length];
                Buffer.BlockCopy(salt, 0, combinedBytes, 0, salt.Length);
                Buffer.BlockCopy(hashBytes, 0, combinedBytes, salt.Length, hashBytes.Length);

                // Convert to Base64 for storage
                return Convert.ToBase64String(combinedBytes);
            }
           
        }

        public static bool ValidatePassword(this string inputPassword, string hashedPassword)
        {
            // Extract salt from the hashed password
            // Convert the stored hash back to bytes
            byte[] storedBytes = Convert.FromBase64String(hashedPassword);

            // Extract salt from the stored hash
            byte[] salt = new byte[128 / 8];
            Buffer.BlockCopy(storedBytes, 0, salt, 0, salt.Length);

            // Derive a 256-bit subkey using PBKDF2 with the provided salt
            using (var pbkdf2 = new Rfc2898DeriveBytes(inputPassword, salt, 100000, HashAlgorithmName.SHA256))
            {
                byte[] hashBytes = pbkdf2.GetBytes(256 / 8); // 256 bits

                // Combine salt and hash and compare with the stored hash
                byte[] combinedBytes = new byte[salt.Length + hashBytes.Length];
                Buffer.BlockCopy(salt, 0, combinedBytes, 0, salt.Length);
                Buffer.BlockCopy(hashBytes, 0, combinedBytes, salt.Length, hashBytes.Length);

                // Compare the generated hash with the stored hash
                var result= storedBytes.SequenceEqual(combinedBytes);
                return result;
            }
        }
    }
}