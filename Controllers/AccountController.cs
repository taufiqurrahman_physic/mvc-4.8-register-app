﻿using RegisterWebApp.Models.ViewModels;
using RegisterWebApp.Repository.Interface;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RegisterWebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserLoginRepo _userLoginRepo;
        private readonly ISelectHelper _selectHelper;

        public AccountController(IUserLoginRepo userLoginRepo, 
                                 ISelectHelper selectHelper)
        {
            _userLoginRepo = userLoginRepo;
            _selectHelper = selectHelper;
        }

        // GET: Account
        public ActionResult Login()
        {
            var model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel data)
        {
            if (ModelState.IsValid)
            {
                var userValidation = _userLoginRepo.ValidateUserLogin(data.Username, data.Password);

                if (!userValidation.Item1)
                {
                    ViewBag.ErrorMessage = userValidation.Item2;
                    return View(data);
                }

                Session["Username"] = userValidation.Item2;
                
                return RedirectToAction("Index", "Home");
            }

            return View(data);
        }

        public async Task<ActionResult> Register()
        {
            var model = new RegisterViewModel();
            model.Roles = await _selectHelper.GetRoles();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel data)
        {
            data.Roles = await _selectHelper.GetRoles(data.RoleIds.FirstOrDefault());

            if (!ModelState.IsValid)
            {
                return View(data);
            }

            var isUsernameExist = await _userLoginRepo.IsUsernameExist(data.Username);
            if (!isUsernameExist.Item1)
            {
                ViewBag.ErrorMessage = isUsernameExist.Item2;
                return View(data);
            }

            var response = await _userLoginRepo.CreateUser(data);
            if (!response.Item1)
            {
                ViewBag.ErrorMessage = response.Item2;
                return View(data);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            if (Session["Username"] != null && !string.IsNullOrEmpty(Session["Username"] as string))
                Session.Remove("Username");

            return RedirectToAction(nameof(Login));
        }

    }
}