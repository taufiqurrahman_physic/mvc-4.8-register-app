﻿using RegisterWebApp.CustomAuthentication;
using RegisterWebApp.Extension;
using RegisterWebApp.Repository.Interface;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RegisterWebApp.Controllers
{
    [CustomAuthenticationFilter]
    public class HomeController : Controller
    {
        private readonly IUserRepo _userRepo;

        public HomeController(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        // GET: Home
        [CustomAuthorize(Roles.ADMIN, Roles.USER)]
        public async Task<ActionResult> Index()
        {
            var model = await _userRepo.GetByUsers();
            return View(model);
        }

        public ActionResult UnAuthorized()
        {
            ViewBag.Message = "You are no authorized !";

            return View();
        }
    }
}