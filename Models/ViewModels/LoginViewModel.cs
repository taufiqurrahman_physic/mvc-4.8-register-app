﻿using System.ComponentModel.DataAnnotations;

namespace RegisterWebApp.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="This field is requiered")]
        [MaxLength(50, ErrorMessage = "The input cant more than 50 character")]
        public string Username { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        public string Password { get; set; }
    }
}