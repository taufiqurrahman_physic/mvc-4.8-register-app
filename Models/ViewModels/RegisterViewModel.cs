﻿using RegisterWebApp.CustomValidationAttribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RegisterWebApp.Models.ViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "This field is requiered")]
        [MaxLength(50, ErrorMessage = "The input cant more than 50 character")]
        public string Username { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        [MaxLength(50, ErrorMessage = "The input cant more than 50 character")]
        [MinLength(8, ErrorMessage = "he input must more than 8 character")]
        public string Password { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        [MaxLength(100, ErrorMessage = "The input cant more than 100 character")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        [MaxLength(100, ErrorMessage = "The input cant more than 100 character")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        [MaxLength(100, ErrorMessage = "The input cant more than 100 character")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage = "This field is requiered")]
        public string Address { get; set; }
        [ListRequired(ErrorMessage = "Role is required")]
        [ListNotDuplicate(ErrorMessage = "Role cant duplicate")]
        public List<string> RoleIds { get; set;}
        public IEnumerable<SelectListItem> Roles { get; set;}
    }
}