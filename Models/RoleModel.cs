﻿namespace RegisterWebApp.Models
{
    public class RoleModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
    }
}