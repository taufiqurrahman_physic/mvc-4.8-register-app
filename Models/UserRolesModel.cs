﻿namespace RegisterWebApp.Models
{
    public class UserRolesModel
    {
        public string Username { get; set; }
        public string Roles { get; set; }
    }
}