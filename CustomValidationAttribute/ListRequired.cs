﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegisterWebApp.CustomValidationAttribute
{
    public class ListRequired : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (value is List<string> list)
            {
                return list.Any(s=> !string.IsNullOrEmpty(s));
            }

            return false;
        }
    }

    public class ListNotDuplicate : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (value is List<string> list)
            {
                if (!list.Any(s => !string.IsNullOrEmpty(s)))
                {
                    return false;
                }

                var isDuplicate = list.GroupBy(s => s).Any(s => !string.IsNullOrEmpty(s.Key) && s.Count() > 1);

                if (!isDuplicate)
                    return true;
            }

            return false;
        }
    }
}