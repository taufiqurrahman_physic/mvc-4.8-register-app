﻿$(document).ready(function () {
    // Set a timeout to hide the alert after 3000 milliseconds (3 seconds)
    setTimeout(function () {
        $("#alertId").fadeOut("slow");
    }, 3000);
});