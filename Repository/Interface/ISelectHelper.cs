﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RegisterWebApp.Repository.Interface
{
    public interface ISelectHelper
    {
        Task<IEnumerable<SelectListItem>> GetRoles(string selectedId= null);
    }
}
