﻿using RegisterWebApp.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RegisterWebApp.Repository.Interface
{
    public interface IUserRepo
    {
        Task<UserModel> GetByUsername(string username);
        Task<IEnumerable<UserModel>> GetByUsers();
    }
}
