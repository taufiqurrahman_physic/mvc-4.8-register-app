﻿using RegisterWebApp.Models.ViewModels;
using System;
using System.Threading.Tasks;

namespace RegisterWebApp.Repository.Interface
{
    public interface IUserLoginRepo
    {
        Tuple<bool,string> ValidateUserLogin(string username, string password);
        Task<Tuple<bool, string>> CreateUser(RegisterViewModel data);
        Task<Tuple<bool, string>> IsUsernameExist(string username);
    }
}
