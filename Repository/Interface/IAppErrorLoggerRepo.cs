﻿using RegisterWebApp.Models.ViewModels;
using System.Threading.Tasks;

namespace RegisterWebApp.Repository.Interface
{
    public interface IAppErrorLoggerRepo
    {
        Task Create(AppErrorLogger data);
    }
}
