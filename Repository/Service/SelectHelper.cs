﻿using Dapper;
using RegisterWebApp.Models;
using RegisterWebApp.Repository.Interface;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace RegisterWebApp.Repository.Service
{
    public class SelectHelper : ISelectHelper
    {
        private readonly string _conString;

        public SelectHelper()
        {
            _conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }

        public async Task<IEnumerable<SelectListItem>> GetRoles(string selectedId = null)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                var roles = await db.QueryAsync<RoleModel>
                                ("GetRoles", commandType: CommandType.StoredProcedure);

                var result = new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Selected = string.IsNullOrEmpty(selectedId),
                        Text = "Select Role",
                        Value = ""
                    }
                };

                if (roles == null || !roles.Any())
                {
                    return result;
                }

                result.AddRange(roles.Select(r => new SelectListItem
                {
                    Selected = selectedId == r.Id,
                    Text = r.RoleName,
                    Value = r.Id
                }));

                return result;
            }
        }
    }
}