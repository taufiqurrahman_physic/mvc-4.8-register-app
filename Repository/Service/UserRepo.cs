﻿using Dapper;
using Extension.Converter;
using RegisterWebApp.Models;
using RegisterWebApp.Repository.Interface;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RegisterWebApp.Repository.Service
{
    public class UserRepo : IUserRepo
    {
        private readonly string _conString;

        public UserRepo()
        {
            _conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }

        public async Task<UserModel> GetByUsername(string username)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                var user = await db.QueryFirstOrDefaultAsync<UserModel>
                                ("GetUserDetail", new { username = username }, commandType: CommandType.StoredProcedure);

                user.DateOfBirth = user.DateOfBirth.UTCToLocal();

                return user;
            }
        }

        public async Task<IEnumerable<UserModel>> GetByUsers()
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                var user = await db.QueryAsync<UserModel>
                                ("GetUsers", commandType: CommandType.StoredProcedure);
                return user;
            }
        }
    }
}