﻿using Dapper;
using Extension.Converter;
using RegisterWebApp.Repository.Interface;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RegisterWebApp.Repository.Service
{
    public class AppErrorLoggerRepo : IAppErrorLoggerRepo
    {
        private readonly string _conString;
        public AppErrorLoggerRepo() 
        {
            _conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }

        public async Task Create(Models.ViewModels.AppErrorLogger data)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                await db.ExecuteAsync("CreateAppErrorLogger",
                    new
                    {
                        controllerName = data.ControllerName,
                        actionName = data.ActionName,
                        exceptionMessage = data.ExceptionMessage,
                        exceptionStackTrace = data.ExceptionStackTrack,
                        exceptionLogTime = data.ExceptionLogTime
                    }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}