﻿using Dapper;
using Extension.Converter;
using RegisterWebApp.Extension.Hashing;
using RegisterWebApp.Models;
using RegisterWebApp.Models.ViewModels;
using RegisterWebApp.Repository.Interface;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace RegisterWebApp.Repository.Service
{
    public class UserLoginRepo : IUserLoginRepo
    {
        private readonly string _conString;

        public UserLoginRepo()
        {
            _conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }

        public async Task<Tuple<bool, string>> CreateUser(RegisterViewModel data)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                await db.ExecuteAsync("CreateUsers",
                    new
                    {
                        username = data.Username,
                        password = data.Password.GetPasswordHashing(),
                        firstName = data.FirstName,
                        lastName = data.LastName,
                        email = data.Email,
                        dateOfBirth = data.DateOfBirth.LocalToUTC(),
                        address = data.Address,
                        roleIds = string.Join(",", data.RoleIds)
                    }, commandType: CommandType.StoredProcedure);
            }

            return new Tuple<bool, string>(true, "Data Created");
        }

        public async Task<Tuple<bool, string>> IsUsernameExist(string username)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                var user = await db.QueryFirstOrDefaultAsync<UserLoginModel>
                                ("GetUserLogin", new { username = username }, commandType: CommandType.StoredProcedure);

                if (user == null)
                {
                    return new Tuple<bool, string>(true, username);
                }

                return new Tuple<bool, string>(false, "Username already exist");
            }
        }

        public Tuple<bool, string> ValidateUserLogin(string username, string password)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                var user = db.QueryFirstOrDefault<UserLoginModel>
                                ("GetUserLogin", new { username = username }, commandType: CommandType.StoredProcedure);

                if (user == null)
                {
                    return new Tuple<bool, string>(false, "Login account not found. Please register first");
                }

                if (user.Username != username || !password.ValidatePassword(user.Password))
                    return new Tuple<bool, string>(false, "Username or passwor is not valid");
            }

            return new Tuple<bool, string>(true, username);
        }
    }
}