USE [master]
GO
/****** Object:  Database [RegisterDB]    Script Date: 12/17/2023 10:51:22 PM ******/
CREATE DATABASE [RegisterDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RegisterDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\RegisterDB.mdf' , SIZE = 270336KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RegisterDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\RegisterDB_log.ldf' , SIZE = 204800KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [RegisterDB] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RegisterDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RegisterDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RegisterDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RegisterDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RegisterDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RegisterDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [RegisterDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RegisterDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RegisterDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RegisterDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RegisterDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RegisterDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RegisterDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RegisterDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RegisterDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RegisterDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RegisterDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RegisterDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RegisterDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RegisterDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RegisterDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RegisterDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RegisterDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RegisterDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RegisterDB] SET  MULTI_USER 
GO
ALTER DATABASE [RegisterDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RegisterDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RegisterDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RegisterDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RegisterDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RegisterDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [RegisterDB] SET QUERY_STORE = ON
GO
ALTER DATABASE [RegisterDB] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [RegisterDB]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [varchar](450) NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoleMapper]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoleMapper](
	[Id] [varchar](450) NOT NULL,
	[RoleId] [varchar](450) NOT NULL,
	[UserId] [varchar](450) NOT NULL,
 CONSTRAINT [PK_UserRoleMapper] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [varchar](450) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[DateOfBirth] [datetime2](7) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UQ_Username] UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[Vw_UserRoles]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_UserRoles]
AS
SELECT u.Username, STRING_AGG(r.RoleName,',') As Roles
FROM [RegisterDB].[dbo].UserRoleMapper rm
INNER JOIN [RegisterDB].[dbo].[User] u ON u.Id = rm.UserId
INNER JOIN [RegisterDB].[dbo].[Role] r ON r.Id = rm.RoleId
GROUP BY u.Username
GO
/****** Object:  View [dbo].[Vw_UserLogin]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Vw_UserLogin]
AS
SELECT u.Username, u.Password
FROM  [RegisterDB].[dbo].[User] u
GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitString]
(
    @String NVARCHAR(MAX),
    @Delimiter CHAR(1)
)
RETURNS TABLE
AS
RETURN
(
    WITH SplitCTE AS
    (
        SELECT
            1 AS Id,
            CAST(NULLIF(CHARINDEX(@Delimiter, @String), 0) AS NVARCHAR(MAX)) AS Pos,
            CASE 
                WHEN CHARINDEX(@Delimiter, @String) > 0
                    THEN SUBSTRING(@String, 1, NULLIF(CHARINDEX(@Delimiter, @String), 0) - 1)
                ELSE
                    @String  -- Return the entire string if no delimiter is found
            END AS Value
        UNION ALL
        SELECT
            Id + 1,
            CAST(NULLIF(CHARINDEX(@Delimiter, @String, Pos + 1), 0) AS NVARCHAR(MAX)),
            SUBSTRING(@String, Pos + 1, NULLIF(CHARINDEX(@Delimiter, @String, Pos + 1), 0) - Pos - 1)
        FROM SplitCTE
        WHERE Pos > 0
    )
    SELECT
        Id,
        Value
    FROM SplitCTE
);
GO
/****** Object:  Table [dbo].[AppErrorLogger]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppErrorLogger](
	[Id] [varchar](450) NOT NULL,
	[ControllerName] [varchar](100) NOT NULL,
	[ActionName] [varchar](100) NOT NULL,
	[ExceptionMessage] [nvarchar](max) NOT NULL,
	[ExceptionStackTrack] [nvarchar](max) NOT NULL,
	[ExceptionLogTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AppErrorLogger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserRoleMapper]  WITH CHECK ADD  CONSTRAINT [FK_UserRoleMapper_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRoleMapper] CHECK CONSTRAINT [FK_UserRoleMapper_Role]
GO
ALTER TABLE [dbo].[UserRoleMapper]  WITH CHECK ADD  CONSTRAINT [FK_UserRoleMapper_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserRoleMapper] CHECK CONSTRAINT [FK_UserRoleMapper_User]
GO
/****** Object:  StoredProcedure [dbo].[CreateAppErrorLogger]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateAppErrorLogger]
	@controllerName varchar(100),
	@actionName varchar(100),
	@exceptionMessage nvarchar(max),
	@exceptionStackTrace nvarchar(max),
	@exceptionLogTime datetime2(7)
AS
BEGIN
	INSERT INTO [RegisterDB].[dbo].[AppErrorLogger]
	(Id,
	ControllerName,
	ActionName,
	ExceptionMessage,
	ExceptionStackTrack,
	ExceptionLogTime)
	VALUES 
	(convert(nvarchar(50), NEWID()),
		@controllerName, 
		@actionName,
		@exceptionMessage,
		@exceptionStackTrace,
		@exceptionLogTime)
END
GO
/****** Object:  StoredProcedure [dbo].[CreateUsers]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateUsers] 
	@username varchar(50),
	@password nvarchar(max),
	@firstName nvarchar(100),
	@lastName nvarchar(100),
	@email nvarchar(100),
	@dateOfBirth datetime2(7),
	@address nvarchar(max),
	@roleIds varchar(max)
AS
BEGIN
	Declare @userId NVARCHAR(50);
	SET @userId = convert(nvarchar(50), NEWID());

	INSERT INTO [RegisterDB].[dbo].[User]
	(Id, Username, Password, FirstName, LastName, Email, DateOfBirth, Address) VALUES
	(@userId, @username, @password, @firstName, @lastName, @email, @dateOfBirth, @address)

	DECLARE @Temp TABLE (RoleId NVARCHAR(450))
	
	-- Insert role IDs into the table variable
	INSERT INTO @Temp (RoleId)
	SELECT value
	FROM dbo.SplitString(@roleIds, ',')
	
	-- Loop through the table variable and insert records into UserRoleMapper
	WHILE (SELECT COUNT(*) FROM @Temp) > 0
	BEGIN
	    -- Declare variable to hold the current role ID
	    DECLARE @roleId NVARCHAR(450)
	    
	    -- Select the top 1 role ID from the table variable
	    SELECT TOP 1 @roleId = RoleId FROM @Temp
	
	    -- Insert into UserRoleMapper
	    INSERT INTO [RegisterDB].[dbo].[UserRoleMapper] ([Id], [RoleId], [UserId])
	    VALUES (CONVERT(NVARCHAR(50), NEWID()), @roleId, @userId)
	
	    -- Delete the processed role ID from the table variable
	    DELETE FROM @Temp WHERE RoleId = @roleId
	END

END
GO
/****** Object:  StoredProcedure [dbo].[GetRoles]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRoles]
AS
BEGIN
	SELECT r.Id, r.RoleName FROM [RegisterDB].[dbo].[Role] r
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserDetail]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserDetail] 
	@username varchar(50)
AS
BEGIN
	SELECT TOP 1 u.* , ur.Roles
	FROM [RegisterDB].[dbo].[User] u
	INNER JOIN Vw_UserRoles ur ON ur.Username = u.Username
	WHERE u.Username = @username
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserLogin]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserLogin] 
	@username varchar(50)
AS
BEGIN
	SELECT TOP 1 * FROM Vw_UserLogin r WHERE r.Username = @username
END
GO
/****** Object:  StoredProcedure [dbo].[GetUserRole]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserRole] 
	@username varchar(50)
AS
BEGIN
	SELECT TOP 1 * FROM Vw_UserRoles r WHERE r.Username = @username
END
GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 12/17/2023 10:51:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUsers]
AS
BEGIN
	SELECT u.* , ur.Roles
	FROM [RegisterDB].[dbo].[User] u
	INNER JOIN Vw_UserRoles ur ON ur.Username = u.Username
END
GO
USE [master]
GO
ALTER DATABASE [RegisterDB] SET  READ_WRITE 
GO

/******* Init Role *******/
 INSERT INTO [RegisterDB].[dbo].[Role]
 (Id,RoleName) VALUES
 (CONVERT(NVARCHAR(50), NEWID()),'Admin'),
 (CONVERT(NVARCHAR(50), NEWID()),'User')