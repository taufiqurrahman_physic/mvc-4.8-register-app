﻿using Dapper;
using Extension.Converter;
using RegisterWebApp.Models.ViewModels;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace Extension.AppCustom
{
    public class CustomExceptionHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        private readonly string _conString;

        public CustomExceptionHandlerAttribute()
        {
            _conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException(nameof(filterContext));
            }

            AppErrorLogger logger = new AppErrorLogger()
            {
                ExceptionMessage = filterContext.Exception.Message,
                ExceptionStackTrack = filterContext.Exception.StackTrace,
                ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                ActionName = filterContext.RouteData.Values["action"].ToString(),
                ExceptionLogTime = DateTime.Now.LocalToUTC()
            };

            Create(logger);

            filterContext.ExceptionHandled = true;
            filterContext.Result = new ViewResult()
            {
                ViewName = "Error"
            };
        }

        private void Create(RegisterWebApp.Models.ViewModels.AppErrorLogger data)
        {
            using (IDbConnection db = new SqlConnection(_conString))
            {
                 db.Execute("CreateAppErrorLogger",
                    new
                    {
                        controllerName = data.ControllerName,
                        actionName = data.ActionName,
                        exceptionMessage = data.ExceptionMessage,
                        exceptionStackTrace = data.ExceptionStackTrack,
                        exceptionLogTime = data.ExceptionLogTime
                    }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}