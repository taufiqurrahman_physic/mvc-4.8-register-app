using Microsoft.Practices.Unity;
using RegisterWebApp.Repository.Interface;
using RegisterWebApp.Repository.Service;
using System.Web.Mvc;
using Unity.Mvc3;

namespace RegisterWebApp
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();            

            container.RegisterType<IUserLoginRepo, UserLoginRepo>();
            container.RegisterType<ISelectHelper, SelectHelper>(); 
            container.RegisterType<IUserRepo, UserRepo>();

            return container;
        }
    }
}