﻿using Extension.AppCustom;
using System.Web.Mvc;

namespace RegisterWebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomExceptionHandlerAttribute());
            filters.Add(new HandleErrorAttribute()); //default
        }
    }
}
