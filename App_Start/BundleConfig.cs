﻿using System.Web.Optimization;

namespace RegisterWebApp.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new Bundle("~/js").Include(
                "~/bower_components/jquery/dist/jquery.js",
                "~/bower_components/bootstrap/dist/js/bootstrap.js",
                "~/Script/AppJqueryExtension.js"
                ));

            bundles.Add(new StyleBundle("~/css").Include(
                "~/bower_components/bootstrap/dist/css/bootstrap.css",
                "~/Content/Site.css",
                "~/Content/AppCssExtension.css"
                ));

            bundles.Add(new StyleBundle("~/login-css").Include(
                "~/Content/LoginStyleExtension.css"
                ));
        }
    }
}